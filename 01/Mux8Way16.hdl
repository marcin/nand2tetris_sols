// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Mux8Way16.hdl

/**
 * 8-way 16-bit multiplexor:
 * out = a if sel == 000
 *       b if sel == 001
 *       etc.
 *       h if sel == 111
 */

CHIP Mux8Way16 {
    IN a[16], b[16], c[16], d[16],
       e[16], f[16], g[16], h[16],
       sel[3];
    OUT out[16];

    PARTS:
    // 1st stage
    Mux16(a=a, b=e, sel=sel[2], out=AorE);
    Mux16(a=b, b=f, sel=sel[2], out=BorF);
    Mux16(a=c, b=g, sel=sel[2], out=CorG);
    Mux16(a=d, b=h, sel=sel[2], out=DorH);
    // 2nd stage
    Mux16(a=AorE, b=CorG, sel=sel[1], out=AorEorCorG);
    Mux16(a=BorF, b=DorH, sel=sel[1], out=BorForDorH);
    // 3rd stage
    Mux16(a=AorEorCorG, b=BorForDorH, sel=sel[0], out=out);
}
